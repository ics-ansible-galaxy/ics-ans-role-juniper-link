# ics-ans-role-juniper-link

Ansible role to add and configure link aggregations on juniper switches.

There are also variables included in the molecule.yml file for molecule prepare step.

This role is not truly idempotent this is due to the limitation of Junos device to show diff for same lines when text has '\n' delimiter. In this case the module junos_linkagg.
https://github.com/ansible/ansible/issues/45289
As such a bool is set on the first task. "when: juniper_link_user|bool"

## Role Variables

```yaml
juniper_link_user: false
juniper_link_login: user
juniper_link_password: vaultedpassword
juniper_link_device_count: 10
juniper_link_lags: []
# - name: ae33
#   description: "test"
#   mode: trunk
#   mtu: 9192
#   members:
#     - "xe-0/0/9:0"
#     - "xe-0/0/9:1"
#   vlans:
#     - name: "blue"
#       vlan_id: 123
#     - name: "green"
#       vlan_id: 321
# - name: ae22
#   description: "test2"
#   mode: trunk
#   mtu: 9192
#   members:
#     - "xe-0/0/8:0"
#     - "xe-0/0/8:1"
#   vlans:
#     - name: "green"
#       vlan_id: 321
# There are also variables set in the molecule file for the molecule prepare step.
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-juniper-link
```

## License

BSD 2-clause
